import {combineReducers} from 'redux';
import {v1} from 'uuid';

const employees = (state = [], action) => {
    switch (action.type) {
        case 'ADD_EMPLOYEE':
            return [...state, {
                name: action.name,
                position: action.position,
                id: v1(),
            }];
        case 'RECEIVE_EMPLOYEES':
            return action.response;
        default:
            return state;
    }
};

const activeDepartment = (prevActiveDepartment = 'all', action) => {
    switch (action.type) {
        case 'FILTER_BY_DEPARTMENT':
            return action.department;
        default:
            return prevActiveDepartment;
    }
};


export const appReducer = combineReducers({
    employees,
    activeDepartment,
});

