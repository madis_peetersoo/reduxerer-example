/*
 If you don't want to lose your data with every refresh, you can easily store it in
 the browser's local storage
 */

export const loadState = () => {
    // It is important to wrap the getItem into try catch because the calls
    // to localStorage.getItem() can fail if the user's browser security
    // settings don't allow calls to the local storage
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};


export const saveState = (state) => {
    // It only works when your state is serializable, but this is
    // the general recommendation in Redux => Your state should always be
    // serializable
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    } catch (err) {
        // Ignore write error currently
        // tho you could log them somewhere
    }
}
