import {describe, it} from 'mocha';
import {expect} from 'chai';
import {filterByDepartment} from './actions';

describe('actions test', () => {
    it('should create an action to receive employees', () => {
        /*
         You should know by know, that an action is a plain javascript object describing
         the change in the state tree. So you'll want to test the action creators and compare
         their returned value with an expected action.
         */
        const department = 'dev';
        const expectedAction = {
            type: 'FILTER_BY_DEPARTMENT',
            department,
        };
        expect(filterByDepartment(department)).to.eql(expectedAction);
    });
});
