import React, {Component, PropTypes} from 'react';

export class Employees extends Component {
    onAddEmployee() {
        const name = this.hireName.value;
        const position = this.hirePosition.value;
        this.hireName.value = '';

        this.props.onClick(name, position);
    }

    onFilter() {
        const filter = this.positionFilter.value;
        this.props.onFilter(filter);
    }
    /*
     TODO: Task 1
     We should store the company's name in the state and use it in the heading
     instead of hard-coding it

     TODO: Task 2
     We need functionality to fire people. You should:
     - add an action creator
       - write a test for it
     - improve the root reducer so that the fired person is removed from the state
       - write a test for it
     - add the fire button here in the front end

     TODO: Task 3
     Implement a loading indicator
     - Just a text 'loading' is good enough as the indicator
     - It should be shown once you start an API call
     - It should be hidden once the API call finishes
     - You shouldn't be able to invoke any new API calls while there is a call in progress
     - Add a "Fetch from server" button to the front end
     - Something in the lines of the following snippet could be useful in the API view
       to mock a delay and the occasional server error
         period = randint(0, 3)
         if period < 2:
         return None
         sleep(period)


     TODO?: Extra for the hyperactive
     Perform on the fly validation for the form using Redux
     - For example, you shouldn't be able to hire a person when a person with tat exact name already exists
     - You shouldn't be able to hire Ivar as designer, because #ivarUX, u know...
     */

    render() {
        return (
            <div>
                <h1>eTag & Roht employees</h1>
                <div>
                    <strong>Department</strong>
                    <select ref={ref => { this.positionFilter = ref; }} onChange={() => this.onFilter()}>
                        <option value="all">All</option>
                        <option value="dev">Developer</option>
                        <option value="des">Designer</option>
                    </select>
                </div>
                {this.props.employees.map(emp =>
                    <li key={emp.id}>{emp.name} [{emp.position}]</li>
                )}
                <input ref={ref => { this.hireName = ref; }} />
                <select ref={ref => { this.hirePosition = ref; }}>
                    <option value="dev">Developer</option>
                    <option value="des">Designer</option>
                </select>
                <button onClick={() => this.onAddEmployee()}>Hire</button>
            </div>
        );
    }
}

Employees.propTypes = {
    onClick: PropTypes.func.isRequired,
    onFilter: PropTypes.func.isRequired,
    employees: PropTypes.array.isRequired,
};

export default Employees;
