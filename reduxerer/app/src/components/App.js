import React, {Component} from 'react';

class TodoApp extends Component {
    render() {
        return (
            <div>
                <button onClick={() => {
                    this.props.store.dispatch({
                        type: 'ADD_EMPLOYEE',
                        name: 'Chuck N',
                    })
                }}>Hire Chuck</button>
                <ul>
                    {this.props.store.getState().map(emp =>
                        <li>{emp.name}</li>
                    )}
                </ul>
            </div>
        )
    }
}

export default TodoApp;
