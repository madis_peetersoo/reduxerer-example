import React, {PropTypes} from 'react';
import {Provider} from 'react-redux';
import EmployeesConnector from './EmployeesConnector';

const Root = ({store}) => (
    <Provider store={store}>
        <EmployeesConnector />
    </Provider>
);

Root.propTypes = {
    store: PropTypes.object.isRequired,
};

export default Root;
