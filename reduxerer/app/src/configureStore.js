import createLogger from 'redux-logger';
import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';


import {appReducer} from './reducers';

const configureStore = () => {
    const appliedMiddleware = applyMiddleware(thunkMiddleware, createLogger());
    const store = createStore(appReducer, appliedMiddleware);

    return store;
};

export default configureStore;
