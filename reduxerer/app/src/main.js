import React from 'react';
import ReactDOM from 'react-dom';

import configureStore from './configureStore';
import Root from './components/Root';
import {fetchEmployees} from './actions';

function init() {
    const store = configureStore();
    store.dispatch(fetchEmployees());

    ReactDOM.render(
        <Root store={store} />,
        document.getElementById('root')
    );
}

export {init};
